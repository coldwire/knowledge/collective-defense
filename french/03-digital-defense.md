## III. Défense numérique
### 1. Connaitre leur outils.
Pourquoi avons-nous besoin de sécurité informatique dans nos millieux ? Pourquoi utilisons-nous des messageries chiffré, des pseudo différents, des VPNs, etc ? Bien entendu nous savons toustes que les gouvernements et entrepises ont des outils pour nous surveiller mais tout cela reste flou pour la plus part d'entre nous. Et pour nous c'est important de mieux connaitre ce as quoi ont fait face pour comprendre comment s'en défendre.

### 2. Messageries
N'utilisez surtout pas les SMS ou les appels téléphonique pour communiquer, c'est un protocol très ancien pas chiffré dont les messages sont très simple à intercepter avec une simple antenne radio. L'état as le control sur les opérateurs internet et peut très bien demander à suivre, écouter et intercepter les SMS d'une personne.

C'est pour cela qu'il est important d'utiliser des messageries chiffré, mais il reste toujours beaucoup de problème lié aux métadonées, à l'addresse IP, la centralisation et la dépendance as des entreprises dont les infrastructure et le code ne sont ni connue ni ouvert (open-source).

* **Session**
  * **Décentralisé**: *oui*
  * **Chiffré**: *oui*
  * **Protection des métadonnées**: *oui*

* **Signal**
  * **Décentralisé**: *non*
  * **Chiffré**: *non*
  * **Protection des métadonnées**: **
### 3. Mot de passe
### 4. Collaboration