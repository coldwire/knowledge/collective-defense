## I. Introduction
### Pourquoi cette brochure ?

> Il y as déjà beaucoup de brochures parlant de sécurité militante mais elles sont très peut souvent mise à jours, n’identifient pas toujours ce contre quoi on se défend.
> Dans cette brochure il y aura aussi beacoup de liens vers des ressources externes ainsi qu’un wiki collaboratif hébergé par notre collectif regroupant tout ce que nous n’avons pas ajouté dans notre brochure.

### Qui somme-nous ?

> Cette brochure est écrite au nom du collectif “coldwire”.
> Nous sommes un groupe indépendant, décentralisé et anonyme construit par des militant·x·e·s, hackers et artistes du monde entier, nous nous sommes donné comme mission de fournire le plus d’informations, de conaissances et d’outils pour permettre aux nouveaux mouvements sociaux et aux collectifs de s’organiser de façon sécurisé, décentralisé et résiliente :·)
