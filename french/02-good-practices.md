## II. Bonne pratiques

Dans beaucoup de brochures, livre ou site web parlant de sécurité militante, on vous donne une simple liste de logiciels "magique" qui vous permettra d'être en "sécurité". Sauf que c'est un myth, quelque-chose de dangereux en fait, croire qu'un VPN vous rend anonyme est faux [1], penser que TOR est infaible l'est aussi [2], un outils dont les données sont chiffré ne veux pas forcément dire "sécurisé" ou anonyme, nous oublions souvent que les "métadonnés" (pseudo, format de fichier, adresse ip, date d'envoie d'un message, etc) ne sont quasiment jamais chiffré et trop facilement accessible tout en donnant assez d'informations pour qu'un attaquant trouve d'autre manière de récupérer des informations sur vous ou lancer des procédures judiciaire [3].

Plus tard nous verrons une liste d'outils, avec leurs + et - et dans quels contexte ils peuvent être utilisés.

### Quelques principes de base
Ces problèmes énoncé dans le paragraphe d'avant sont lié au outils utilisé mais peuvent en partie être évité en suivant des règles de sécurité strict, si vous êtes un-e activiste à risque, un-e journaliste en fuite, il devient très important de suivre ces règles à la lettre:

1. *La sécurité est collective*: quand on travaille à plusieurs, n'importe qui peut mettre en péril le groupe, c'est pour cela qu'il est important de rappeler les règles qui vont suivre et de les suivre **à la lettre**
2. *La sécurité n'est pas une paranoia*: non une personne de vous suit probablement pas h24, non vous n'êtes probablement pas sur écoute, non vos accès internet ne sont pas enregigstré sur les serveurs du gouvernement; la sécurité militante c'est toujours "au cas où", tant que l'on reste discret et que nous nous fondons dans la masse, il y a peut de chance d'être surveillé, mais ça ne veux pas dire qu'il faut rien faire contre.
3. *La sécurité est indispensable pour ne pas finir en procès dès la permière action*: si on est pas infiltré et surveillé on ne se fera pas choper dès le début d'une action et si on laisse peut de traces, on as des chance de pas être retrouvé et surveillé.
4. *La sécurité n'est jamais infaible mais plus on en sait on réduit les chance d'avoir des problèmes*: Les outils et les algorithme de chiffrement peuvent très bien avoir des failles de sécurité, les personnes avec qui l'ont travaille ou qui nous paraissent proche peuvent très bien donner des informations à la police, etc mais si les informations sont bien isolé entre les groupes, que tout le monde à bien suivit cette brochure à la lettre, les dégats devrais être minimes.
5. *La sécurité c'est ne pas sortir du lot*: Être "normale", on aime toustes avoir des stickers, des habits différents, etc mais c'est souvent ce qui fait de nous des personnes de choix à prendre pour cible, adaptez vous à votre environement, fondez vous dans le décors et les chances d'être surveillé seront plus faible.

### Quelques mesures simples pour être moins identifiable
Nous avons parlé plus tôt des "métadonnés", ce sont des donnés de descriptions, comme le nom d'un fichier, sa taille, la date d'édition, mais aussi un nom d'utilisateur-ice, une date d'envoie de message, l'adresse IP de l'envoyeur-euse ou de lae receveur-euse, etc.
Même si le contenue de votre fichier, message, etc est chiffré ces données peuvent servir à vous tracer, comprendre qui vous êtes et ce que vous faite (voir source n°3). Pour éviter cela quelques mesures peuvent être prisent et doivent être suivie à la lettre:

* *Utiliser un pseudo, pas le même selon les endroits et context*: ça évite que la police ou des fachos savent par où vous ête passé si iels trouvent des informations sur vous et un de vos pseudos, permet aussi d'éviter se s'exposer trop au niveau de la justice si un lien est fait avec un pseudo.
* *Séparer vos différentes activités*: Que ce soit sur les réseaux sociaux, vos applications de chat en créant différentes "identités" avec des mails différent ainsi que des VPNs/Proxy dédié à chacune de ces "identités", on verra plus tard comment mettre ça en place d'un point de vue logiciel.
* *Ne pas donner d'information personnels*: Age, nom, adresse, où vous étudiez ou travaillez, le nom de vos proches, etc, ne faite confiance à personne trop vite; permet d'éviter les fuites d'informations.
* *Ne pas parler de vos activitées en dehors des cercles concernés*: et encore moins sur les réseaux sociaux ou en publique, votre égo devras attendre et ne peut que mettre en dangers les autres personnes d'un groupe, vous inclus.
* *Sécurisez vos mot de passes*: Les mot de passe générés sont les meilleurs avec des caractère spéciaux tel que "_&*-^#~" et long de 8 à 64 caractères, utilisez un gestionaire de mot de passe comme lesspass (génération pas mot clé), keypass ou bitwarden.
* Ne stockez aucun contact ou numéro de telephones de militant-x-es sur votre téléphone et n'envoyer surtout pas SMS, plus d'informations sur la parite "Défense digital"

### Sources
[3] [Réputé sûr, Protonmail a livré à la police des informations sur des militants climat](https://reporterre.net/Repute-sur-Protonmail-a-livre-a-la-police-des-informations-sur-des-militants-climat)