﻿Logiciel :  

*   Messagerie annonyme et sécurisé:  
    *   Session : [https://getsession.org/download](https://getsession.org/download) (très safe mais pas parfait pour tout les usages)  
        
    *   Briar : [https://briarproject.org/](https://briarproject.org/) (plutôt moche/pas super simple à comprendre quand on est pas callé en info mais parfait niveaux sécu + resistance a des blackout internet)  
        
    *   Signal : [https://signal.org/](https://signal.org/fr/#signal) (très bon pour usage de tout les jours mais n'est pas annonyme/décentralisé et hebergé aux état-unis par des GAFAM)  
        
    *   Telegram : [https://telegram.org/](https://telegram.org/) (parfait pour usage perso, rien de milittant, rien est chiffré/chiffrement dégueu, sous haute surveillance)pensser a ne pas rendre accesible au autres le numero de tell.  
        
*   Ripple : [https://guardianproject.info/fr/apps/info.guardianproject.ripple/](https://guardianproject.info/fr/apps/info.guardianproject.ripple/) Permet d'effectuer des actions (suppression de donné, cacher l'app, etc) sur des applications compatible en cas de "panique"  
    
*   Pour les recherches internet :  
    *   Tor : [https://www.torproject.org/fr](https://www.torproject.org/fr) (pas tout le temps safe, faire attention aux serveur par lesquels on passent (serveur d'entré et de sortie))  
        
    *   VPN : [https://riseup.net/fr/vpn](https://riseup.net/fr/vpn) (basé sur un protocole plutôt obsolette mais marche bien et gratuit)  
        
    *   Shadowshock, (pas comme un vpn (proxy) mais très puissant et utilisent des methodes de chiffrement plus récentes que ceux de riseup/bitmask mais besoin de setup son propre serveur ou en trouver de confiance) : [https://shadowsocks.org/en/index.htm](https://shadowsocks.org/en/index.html)  
        
*   cryptpad : [https://cryptpad.fr/index.html](https://cryptpad.fr/index.html) (Super bien pour remplacer framapad, etc, chiffrement de bonne qualité reste centralisé mais avec possibilité d'y heberger pas soit même)  
    
*   Lesspass : [https://www.lesspass.com/](https://www.lesspass.com/#/) (bon générateur/gestionaire de mdp sans stockage avec mot clés)  
    

  

Mail :  

*   protonmail + tor (Protonmail est un allier mais reste une entreprise régie par la loi, donc mieux d'y utiliser avec tor/vpn pour éviter les fuites d'adresse ip, etc)  
    
*   Riseup + VPN : [https://riseup.net/fr/email/settings/mail-accounts#comment-faire-pour-cr%C3%A9er-un-compte-courriel](https://riseup.net/fr/email/settings/mail-accounts#comment-faire-pour-cr%C3%A9er-un-compte-courriel) (Nécésite l'invitation d'une personne déja sur la plateforme, est herbergé aux US à Seattle donc régit par le "patriot act" mais géré par des copains anar qui font tout pour déffendre nos donnés <3)  
    
*   Différence entre protonmail et riseuo : le protocole mail de protonmail (le contenu des mails) est chiffré mais Riseup ne l'est pas, proton protège mieux le contenu des messages que riseup qui lui protège mieux les identités si les précautions suffisantes sont prises.  
    
*   Tutanota : [https://tutanota.com](https://tutanota.com) (très cool pour usage perso/remplacer gmail, etc)  
    

  

Sur PC vous pouvez utiliser des mdp pour chiffrer un documents en le zip ==> les algorithmes sont un peu nazes. Le mieux reste de stocker le moins possible sur les disques durs des ordis, surtout s'ils ne sont pas chiffrés. Plutôt préférer stocker les données sur un moyen externe chiffré (clé USB ou carte SD par exemple, sont plus faciles à cacher qu'un disque dur externe).  

Tails = Système d'exploitation (Comme Windows, Mac OS, Linux,...) basé sur Linux orienté sur l'anonymat. Il s'installe sur un périphérique externe comme une clé USB et permet d'utiliser un ordinateur sans laisser de traces sur son disque dur. Pas du tout pratique pour la vie de tous les jours mais de manière ponctuelle pour des trucs sensibles ça peut être une bonne idée.  

ce protéger contre le fichage par empreinte informatique de l'appareil :[https://coveryourtracks.eff.org](https://coveryourtracks.eff.org)  

  

Quelques principes en sécurité :  

*   La sécurité est collective  
    
*   La sécurité n'est pas une paranoia  
    
*    La sécurité est indispensable pour ne pas finir en procès dès la permière action  
    
*   La sécurité n'est jamais infaible mais plus on en as plus on réduit les chance d'avoir des problèmes  
    
*   La sécurité c'est ne pas sortir du lot, donc paraisser comme tout le monde.  
    

  

Quelques mesures simples pour être plus annonyme :  

*   Utiliser un pseudo, et pas le même selon les endroits et context (pas obligatoire mais mieux)  
    
*   Séparer sa vie privé et vie militante (RS, mails)  
    
*   Ne pas donner d'information personnels age , nom , adresse , dans quelle lycée vous étudier , le nom de vos parents ...  
    
*   Ne pas dire partout que vous participer a des actions (Messagerie , RS)  
    
*   Sécuriser vos mdp (8-32 caractère, les mdp généré sont les mieux, utilisez un gestionaire de mdp comme lesspass ou keypass)  
    
*   Ne stocker pas de contacte ou numéro de telephones de militants sur votre tell et n'envoyer pas sms  
    

  

Avant d'allez en action :  

*   Nom d'avocat plus son barreau  
    
*   Vider son telephone (images , applications , contacts) vous pouver faire des copie avec smart switch ou autres pour netoyer totallement votre telephone puis le restitituer (ou ne pas le prendre)  
    
*   Prendre de quoi ce soignier (pansement désinfectant , serum phi)  
    

Ce protéger en action ou manif :  

  
Garde a vus :  

  

Pour les images :  

\=> flouter les pour empecher de ficher les militants  

logiciel : [https://everestpipkin.github.io/image-scrubber/](https://everestpipkin.github.io/image-scrubber/) (en ligne ou hors ligne et efface aussi les méta donée des images)  

Manif :  

Nettoyer sont tell  

former des binome  

s'habillet de la tête au pieds  

masque FP2  

connaitre le nom de la lt  

lors des charges ou mouvement de foule on marche on court pas (on peut ce mettre derrière des obstacle pour ce protéger des mouvement de foule)  

faire attention au charge sur les coté rue adjacente qui peuvent couper le cortège et vous nasser  
  

  

Les canon a eau avec colorant :  

*   Les marqueurs sont de parfaits mouchards. D'abord, ils ont la mémoire longue puisqu'ils persistent trois à quatre semaines sur la peau et même plusieurs mois sur les vêtements en dépit des lavages. Ensuite, ils s'immiscent partout : "Les 'black blocs' ont beau s'équiper de pied en cap, avec lunettes, cagoules et gants, puis se changer très vite pour se fondre dans la foule, il suffit que quelques centimètres de peau aient été en contact avec les PMC pour qu'il soit possible de les confondre ultérieurement". Enfin, les PMC ne se trompent jamais, car le codage propre à chaque marqueur est unique.  
    

S'en protéger :  

*   Il n'existe pour l'instant aucune solution pour enlever les PCM connu. Il faut donc éviter au maximum les canon a eau porter des vetement sur tout le corps de préférence imperméable type k-way noir. Si vous avez été asperger vous pouvez théoriquement vérifier si il y a du colorant a l'aide d'une lampe UV  
    

Le Bornage  :  

Votre telephones ce connecte en permanence a des bornes de réseau (internet, telphonique), ces bornes enregistres des données sur les connections qui garder pendant plusieur années. Elle permettent de savoir par ou vous êtes passé ou allez.  

S'en protégrer :  

*   Ne pas avoir sont telphone en action  
    
*   Laisser en mode avions au maximum sont telephone  
    
*   Paramétrer sont telephone pour qu'il ce connecte a la 2 G ou 3G plutot qu'a H+ ou 4G qui permettent d'avoir un bornage plus précis d'ou vous étiez.  
    

les pass navigo ou autres cartes de transport nominatif :  

*   A chaque fois que vous valider votre pass vous donner l'information que vous êtes a cette endroit et donc si vous faites un action vous prouver votre présence a celle ci.  
    

S'en protégrer :  

*   Acheter des ticket ou frauder ou éviter de prendre des transport juste accoté du lieux marcher jusqu'a la station d'après.  
    

Vérifier qu'on n'est pas suivit par un RG :  

*   Un rg c'est un flic en civil qui cherche a vous suivre / espioner . Le meilleur moyen de savoir si vous êtes suivit c'est de faire des déplacement que personnes d'autres pourrait faire et donc si quelqu'un le fait c'est que c'est un rg. Ce sont souvent des hommes de plus de 25 ans mais cela peut être aussi des femmes, iels peuvent paraitre assez jeunes surtout quand iels veulent infiltrer ou surveiller des mouvements jeunes.  
    

Exemple :  

*   Le métro : sorter de votre ram du métro , allez dans le quai d'en façe (regarde qui sort et qui fait la même chose que vous discraitement) faite une station descender reprener le qu'ai d'enface (de même regarder qui fait la même chose , si quelqu'un a fait ses deux chose c'est probable que vous soyer suivit)  
    
*   Dans la rue : Faite un tour ou 2 d'un paté de maison si quelqu'un fait la même chose c'est clairement louche.  
    
*   Si vous voulez faire fuir un rg faite lui comprendre que vous l'avez cramer en le filmant en général iels parte rapidement.  
    

Sonde du réseau :  

*   Les sonde du réseau sont des dispositif informatique si scan une partie du réseau des telecomunication fraçais , il est automatiser pour détecter les comportements annormal que pourrais faire des militants.  
    

S'en protéger :  

*   Comme d'ahbitude il ne faut pas sortir du lot donc si vous devez par exemple allez dans un squat ou lieux militant éviter d'avoir votre telephone alumé (50 telephone dans un hangar sa fait mouche). Eviter aussi d'éteindre tous d'un coup votre telephone (pratique très courante lors d'ag ou d'action) car c'est très suspec eteignier les petits a petits et pas forcément au même endroit.  
    

  

 ,, les drone, ,les camera ,reconaisance faciale  

  

les limites de la sécu :  

En réalité, faute de Bunker dans la forêt enchantée, nos réunions les plus importantes ont lieu dans les espaces que l’on juge les plus sécurisés. En général, on se dit que les locaux dont on connait les personnes qui en possèdent les clés sont les plus sécurisés. Pourtant, un local syndical utilisé depuis 20 ans est il vraiment un lieu sécur ? Un squat où cohabitent 40 personnes qui laissent entrer tou·tes leurs "ami·es" est-il sécur ? Son appart qu’on a mis quelques fois en location sur AirBnB pour payer ses factures est-il secur ? Qui est vraiment cette personnne qui est venue installer la fibre chez nous ? Il ne s’agit pas de rentrer dans la paronoïa, simplement de se dire qu’en physique comme en ligne, penser maîtriser 100% de la sécurité est illusoire. Il faut faire des compromis si on ne veut pas s’organiser tout seul avec soi même.  

**Résidu des donnée:**

Un problème de sécurité peu connue est que lorsqu'on supprime des données informatiques sur un PC ou un portable: les données ne sont pas vraiment supprimés mais l'espace ou les données était occupés est juste marqués "vide". C'est pour ca, supprimer est beaucoup plus rapide que copier.

Ceci pose un problème de sécurité car si votre PC ou portable ne décide pas d'écrire sur cette espace marqué comme vide, c'est possible de récupperer ces données supprimer en utilisant des outilles comme "GetDataback". Cette outille est fréquement utilisé par des experts durant des analyses de type "forensics" sur les équipement éléctronique.  
**Protection:**  

Pour les PC c'est beaucoup plus simple que les téléphones on doit utiliser une de plusieurs outille qui réécrit des bits 0 sur tous les espaces marqués "vide", par exemple:  

\- mon logiciel recommandé pour ce problème: Eraser (https://eraser.heidi.ie/). Vous pouvez l'installer en deux modes, mode normal et mode portable (sur une clé USB pour l'utiliser sur plusieurs PC) ce qui est utile dans des cas particuliers. C'est important de configurer ce logiciel, en allant dans Édit > Préférences > Erasing et dans l'onglet Files choisissent la méthode pour effacer les fichiers. La méthode Gutmann est la plus sécurisé mais en standard international US Dod 5220.22-M est suffisant.  

\- Secure Eraser est similaire à Eraser mais il a aussi la fonctionnalité de faire une opération pour supprimer tous l'espace marqué comme "vide" pour rendre impossible de retrouver des fichiers déjà supprimer. Cette opération peut ce faire pour un disque dur externe, une clé USB, une SD card...etc.

N.B.: ces processus peuvent prendre beaucoup de temps, soyez patient

N.B.: ces logiciels sont des logicielles Windows, je n'ai pas d'expérience sur les machines Chromebook, Mac, Linux...etc.

Pour les portables c'est un peu plus difficile:

\- la méthode la plus simple et celle que je recommande est de stocker toutes les informations sensibles sur une SD card. Et puis la connectait à un ordi et utiliser Secure Eraser ou de faire un root ou un jailbreak et utiliser Secure Eraser directement sur votre portable.  

\- Utilisation de l'application Ishredder pour android et apple ([https://www.protectstar.com/en/products/ishredder-android](https://www.protectstar.com/en/products/ishredder-android)) qui permet d'éffacer les fichiers d'une manière sécurisée (comme Eraser).

**DNS leak:**

Une erreure commun pour les gens qui essaie d'utiliser un VPN/Tor est le "DNS leak". C'est quand les requêtes DNS  ne passent pas par le VPN ce qui premet à votre FAI de savoir quel site vous y accéder et quand.

Pour ça c'est important de s'assurer que le DNS n'est pas statique comme google DNS par exemple et que le routeur ou le serveur dns de votre choix ne vous log pas. Normalement l'utilisation du navigateur TOR est suffisante pour se sécuriser contre un "DNS leak" mais l'utilisation d'un serveur DNS publique que vous faisait confiance est toujours mieux ou même un petit serveur DNS local perso.

La plupart des serveurs DNS qui était fait par des anarchistes ou gauchiste révolutionaire ne fonctionne pas ajd si vous connaiser des serveurs DNS publique que vous le fait confiance n'hésitez pas à me le dire.